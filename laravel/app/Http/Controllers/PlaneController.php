<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Plane;

class PlaneController extends Controller
{
    public function index()
    {
    	$planes = Plane::getAllPlanes();
    	return view('narain.list_view',compact('planes'));
    }

     public function show($id)
    {
    	$plane = Plane::getPlane($id);
    	return view('narain.detailed_view',compact('plane'));
    }

}
