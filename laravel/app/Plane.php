<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plane extends Model
{
    public static function getAllPlanes()
    {
    	return self::all();
    }

    public static function getPlane($id)
    {
    	return self::where('id',$id)->first();
    }
}
