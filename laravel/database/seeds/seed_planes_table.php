<?php

use Illuminate\Database\Seeder;


class seed_planes_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('planes')->insert([

        	'name' => 'Boeing 737',
        	'description' => 'Manufactured by Boeing Commercial planes. First flight in 1967',
        	'image' => '737.jpg'
        ]);

        DB::table('planes')->insert([

        	'name' => 'Boeing 747',
        	'description' => 'Manufactured by Boeing Commercial planes',
        	'image' => '747.jpg'
        ]);

		
        DB::table('planes')->insert([

        	'name' => 'Boeing 767',
        	'description' => 'Manufactured by Boeing Commercial planes',
        	'image' => '767.jpg'
        ]);


        DB::table('planes')->insert([

        	'name' => 'Boeing 777',
        	'description' => 'Manufactured by Boeing Commercial planes',
        	'image' => '777.jpg'
        ]);


        DB::table('planes')->insert([

        	'name' => 'Boeing 787',
        	'description' => 'Manufactured by Boeing Commercial planes',
        	'image' => '787.jpg'
        ]);


    }
}
